# Discount Computation API

#List of commands

Clone from git : git clone https://<username>:<password>@bitbucket.org/somuradhakrishnan/discount-computation-api.git

Build          : mvn clean package [Note: This will excecute unit test cases and run code coverage also]

run            : java -jar ./target/discount-computation-api-0.0.1-SNAPSHOT.jar


#Required file location

1. After excecuting build code coverage html can be found inside target/site/jacoco/index.html

2. UML file (discount-computation-api.umlcd.png) is inside project root directory


#Swagger details [Note: Swagger is used to Document the API]

http://<hostname>:8080/api/v1/swagger-ui.html

Endpoint : /getDiscountDetails

#Sample request:
```json
{
  "billDto": [
    {
      "id": 0,
      "priceOfEach": 99,
      "productName": "MONITOR",
      "quantity": 10,
      "totalPrice": 990
    }
  ],
  "billNo": "A123",
  "total": 990,
  "user": "Aliko"
}
```
#Sample response:
```json
{
  "user": "Aliko",
  "billNo": "A123",
  "discountDTO": [
    {
      "id": 0,
      "priceAfterDiscount": 648,
      "appliedDiscountInfo": "Employee discount 30% + $5 for every $100"
    }
  ],
  "total": 648
}
```

#H2 Database details

Note: Module is implementeed with H2 in-memory database

H2 DB console URL : http://<hostname>:8080/api/v1/h2-console/login.jsp
JDBC URL : jdbc:h2:mem:testdb  
Username : sa  
Password : password  

 
 




