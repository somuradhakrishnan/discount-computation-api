package com.poc.discountcomputationapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jmx.JmxAutoConfiguration;

/**
 *
 * @author Soma
 */

@SpringBootApplication
@EnableAutoConfiguration(exclude = { JmxAutoConfiguration.class })
public class DiscountComputationApiApplication {

	public static void main(String[] args) {
		System.setProperty("server.servlet.context-path", "/api/v1");
		SpringApplication.run(DiscountComputationApiApplication.class, args);

	}

}
