package com.poc.discountcomputationapi.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.poc.discountcomputationapi.dto.PostDTO;
import com.poc.discountcomputationapi.dto.ResponseDTO;
import com.poc.discountcomputationapi.service.ComputationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Soma
 */

@RestController
public class ComputationController {
	
	private static final Logger logger = LoggerFactory.getLogger(ComputationController.class);

	@Autowired
	ComputationService computationService;

	@RequestMapping(value = "/getDiscountDetails", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO> getDiscountDetails(@RequestBody @Valid PostDTO postDto) {
		logger.info("Request received for bill no: {}", postDto.getBillNo());
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<>(computationService.getDiscountDetails(postDto), headers, HttpStatus.OK);
	}

}
