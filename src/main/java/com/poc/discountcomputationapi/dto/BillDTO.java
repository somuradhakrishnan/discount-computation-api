package com.poc.discountcomputationapi.dto;

import java.math.BigDecimal;

/**
*
* @author Soma
*/

public class BillDTO {
	
	private int id;
	
	private String productName;
	
	private int quantity;
	
	private BigDecimal priceOfEach;
	
	private BigDecimal totalPrice;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPriceOfEach() {
		return priceOfEach;
	}

	public void setPriceOfEach(BigDecimal priceOfEach) {
		this.priceOfEach = priceOfEach;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

}
