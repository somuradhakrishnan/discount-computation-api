package com.poc.discountcomputationapi.dto;

import java.math.BigDecimal;
import java.util.List;

/**
*
* @author Soma
*/

public class DiscountDTO {

	private int id;
	private BigDecimal priceAfterDiscount;
	private String appliedDiscountInfo;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BigDecimal getPriceAfterDiscount() {
		return priceAfterDiscount;
	}

	public void setPriceAfterDiscount(BigDecimal priceAfterDiscount) {
		this.priceAfterDiscount = priceAfterDiscount;
	}

	public String getAppliedDiscountInfo() {
		return appliedDiscountInfo;
	}

	public void setAppliedDiscountInfo(String appliedDiscountInfo) {
		this.appliedDiscountInfo = appliedDiscountInfo;
	}

}
