package com.poc.discountcomputationapi.dto;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.constraints.NotEmpty;

import com.poc.discountcomputationapi.annotations.ProductConstraint;
import com.poc.discountcomputationapi.annotations.UserNameConstraint;

/**
*
* @author Soma
*/

public class PostDTO {

	@NotEmpty(message = "Please provide a bill number")
	private String billNo;
	
	@UserNameConstraint
	private String user;
	
	@ProductConstraint
	private List<BillDTO> billDto;
	
	
	private BigDecimal total;

	public String getBillNo() {
		return billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public List<BillDTO> getBillDto() {
		return billDto;
	}

	public void setBillDto(List<BillDTO> billDto) {
		this.billDto = billDto;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

}
