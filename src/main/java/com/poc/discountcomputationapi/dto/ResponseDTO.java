package com.poc.discountcomputationapi.dto;

import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author Soma
 */

public class ResponseDTO {

	private String user;
	private String billNo;
	private List<DiscountDTO> discountDTO;
	private BigDecimal total;

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public List<DiscountDTO> getDiscountDTO() {
		return discountDTO;
	}

	public void setDiscountDTO(List<DiscountDTO> discountDTO) {
		this.discountDTO = discountDTO;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public String getBillNo() {
		return billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

}
