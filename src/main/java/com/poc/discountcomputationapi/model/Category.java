package com.poc.discountcomputationapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This class will represent product and its attributes: - ID - Name - Priority
 * - Discount_Formula - Promotions - Discount_Description
 * 
 * @author Soma
 */
@Entity
@Table(name = "categories")
public class Category {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "priority", nullable = false)
	private int priority = -1;

	@Column(name = "discount_formula", nullable = false)
	private String discountFormula;

	@Column(name = "promotions", nullable = false)
	private String promotions;

	@Column(name = "discount_description", nullable = false)
	private String discountDescription;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getDiscountFormula() {
		return discountFormula;
	}

	public void setDiscountFormula(String discountFormula) {
		this.discountFormula = discountFormula;
	}

	public String getPromotions() {
		return promotions;
	}

	public void setPromotions(String promotions) {
		this.promotions = promotions;
	}

	public String getDiscountDescription() {
		return discountDescription;
	}

	public void setDiscountDescription(String discountDescription) {
		this.discountDescription = discountDescription;
	}

}
