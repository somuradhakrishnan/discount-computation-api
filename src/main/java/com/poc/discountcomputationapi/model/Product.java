package com.poc.discountcomputationapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This class will represent product and its attributes:
 * - ID
 * - Name
 * - Is_Grocery
 * - Has_Discount
 * - Has_Promotions
 * 
 * @author Soma
 */
@Entity
@Table(name = "products")
public class Product {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "is_grocery", nullable = false)
    private Boolean isGrocery= false;
  
    @Column(name = "has_discount", nullable = false)
    private Boolean hasDiscount= false;
  
    @Column(name = "has_promotions", nullable = false)
    private Boolean hasPromotions= false;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsGrocery() {
		return isGrocery;
	}

	public void setIsGrocery(Boolean isGrocery) {
		this.isGrocery = isGrocery;
	}

	public Boolean getHasDiscount() {
		return hasDiscount;
	}

	public void setHasDiscount(Boolean hasDiscount) {
		this.hasDiscount = hasDiscount;
	}

	public Boolean getHasPromotions() {
		return hasPromotions;
	}

	public void setHasPromotions(Boolean hasPromotions) {
		this.hasPromotions = hasPromotions;
	}

}
