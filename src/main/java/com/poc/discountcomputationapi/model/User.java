package com.poc.discountcomputationapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This class will represent user and its attributes:
 * - ID
 * - First_Name
 * - Last_Name
 * - Is_Employee
 * - Is_Affiliate
 * - Customer_Tenure
 * 
 * @author Soma
 */
@Entity
@Table(name = "users")
public class User {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;
  
    @Column(name = "is_employee", nullable = false)
    private Boolean isEmployee= false;
  
    @Column(name = "is_affiliate", nullable = false)
    private Boolean isAffiliate= false;
  
    @Column(name = "customer_tenure", nullable = false)
    private int customerTenure= 0;
    
    
    public User() {}

    public User(String firstName, String lastName, Boolean isEmployee, Boolean isAffiliate,int customerTenure) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.isEmployee = isEmployee;
        this.isAffiliate = isAffiliate;
        this.customerTenure = customerTenure;
    }


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Boolean getIsEmployee() {
		return isEmployee;
	}

	public void setIsEmployee(Boolean isEmployee) {
		this.isEmployee = isEmployee;
	}

	public Boolean getIsAffiliate() {
		return isAffiliate;
	}

	public void setIsAffiliate(Boolean isAffiliate) {
		this.isAffiliate = isAffiliate;
	}

	public int getCustomerTenure() {
		return customerTenure;
	}

	public void setCustomerTenure(int customerTenure) {
		this.customerTenure = customerTenure;
	}

	

}
