package com.poc.discountcomputationapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.poc.discountcomputationapi.model.Category;

/**
*
* @author Soma
*/
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
	
	@Query("SELECT u FROM Category u WHERE u.name = ?1")
	Category findCategoryByName(String name);
	

}
