package com.poc.discountcomputationapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.poc.discountcomputationapi.model.Product;

/**
*
* @author Soma
*/
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
	
	@Query("SELECT u.name FROM Product u")
	List<String> findAllProductNames();
	
	@Query("SELECT u FROM Product u WHERE u.name = ?1")
	Product findProductByName(String name);

}
