package com.poc.discountcomputationapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.poc.discountcomputationapi.model.User;

/**
 *
 * @author Soma
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
	@Query("SELECT u FROM User u WHERE u.firstName = ?1")
	User findUserByName(String firstName);
	
	@Query("SELECT u.firstName FROM User u")
	List<String> findAllUserNames();
}
