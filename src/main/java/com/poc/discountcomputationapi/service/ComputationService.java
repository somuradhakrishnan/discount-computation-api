package com.poc.discountcomputationapi.service;

import java.util.List;

import com.poc.discountcomputationapi.dto.BillDTO;
import com.poc.discountcomputationapi.dto.DiscountDTO;
import com.poc.discountcomputationapi.dto.PostDTO;
import com.poc.discountcomputationapi.dto.ResponseDTO;
import com.poc.discountcomputationapi.model.Category;

/**
*
* @author Soma
*/

public interface ComputationService {
	
	Category getDiscountFormulas(String categoryName);
	
	ResponseDTO getDiscountDetails(PostDTO postDTO);
	
	List<DiscountDTO> calculateDiscount(String category,List<BillDTO> billDTO);

}
