package com.poc.discountcomputationapi.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Service;

import com.poc.discountcomputationapi.dto.BillDTO;
import com.poc.discountcomputationapi.dto.DiscountDTO;
import com.poc.discountcomputationapi.dto.PostDTO;
import com.poc.discountcomputationapi.dto.ResponseDTO;
import com.poc.discountcomputationapi.model.Category;
import com.poc.discountcomputationapi.model.User;
import com.poc.discountcomputationapi.repository.CategoryRepository;
import com.poc.discountcomputationapi.repository.ProductRepository;
import com.poc.discountcomputationapi.repository.UserRepository;

/**
*
* @author Soma
*/
@Service
public class ComputationServiceImpl implements ComputationService{

	@Autowired
	CategoryRepository categoryRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	ProductRepository productRepository;
	
	private static final String EMPLOYEE = "EMPLOYEE";
	private static final String AFFILIATE = "AFFILIATE";
	private static final String LOYALTY_CUSTOMER = "LOYALTY_CUSTOMER";
	private static final String FOR_ALL = "FOR_ALL";
	
	private BigDecimal totalBillAfterDiscount = new BigDecimal(0);
	
	@Override
	public Category getDiscountFormulas(String categoryName) {
		return categoryRepository.findCategoryByName(categoryName);
	}

	@Override
	public ResponseDTO getDiscountDetails(PostDTO postDTO) {
		totalBillAfterDiscount = new BigDecimal(0);
		User userDetails = userRepository.findUserByName(postDTO.getUser());
		ResponseDTO responseDTO = new ResponseDTO();
		responseDTO.setUser(postDTO.getUser());
		responseDTO.setBillNo(postDTO.getBillNo());
		
		if(userDetails.getIsEmployee()) {
			responseDTO.setDiscountDTO(calculateDiscount(EMPLOYEE,postDTO.getBillDto()));
		}else if(userDetails.getIsAffiliate()) {
			responseDTO.setDiscountDTO(calculateDiscount(AFFILIATE,postDTO.getBillDto()));
		}else if (userDetails.getCustomerTenure()>2) {
			responseDTO.setDiscountDTO(calculateDiscount(LOYALTY_CUSTOMER,postDTO.getBillDto()));
		}else {
			responseDTO.setDiscountDTO(calculateDiscount(FOR_ALL,postDTO.getBillDto()));
		}
		responseDTO.setTotal(totalBillAfterDiscount);
		return responseDTO;
	}

	@Override
	public List<DiscountDTO> calculateDiscount(String categoryName, List<BillDTO> billDTO) {
		
		StandardEvaluationContext context = new StandardEvaluationContext();
		ExpressionParser expressionParser = new SpelExpressionParser();
		Category category = categoryRepository.findCategoryByName(categoryName);
		List<DiscountDTO> discountDTOList = new ArrayList<DiscountDTO>();
		billDTO.stream().forEach(req ->{
			DiscountDTO discountDTO = new DiscountDTO();
			discountDTO.setId(req.getId());
			context.setVariable("amount",req.getTotalPrice());
			BigDecimal priceAfterDiscount = req.getTotalPrice();
			if(!productRepository.findProductByName(req.getProductName()).getIsGrocery() && !categoryName.equals(FOR_ALL))
		          priceAfterDiscount =  (BigDecimal) expressionParser.parseExpression(category.getDiscountFormula()).getValue(context);
			BigDecimal additionalDiscount =  (BigDecimal) expressionParser.parseExpression(category.getPromotions()).getValue(context);
			BigDecimal totalDiscount = priceAfterDiscount.subtract(additionalDiscount);
			totalBillAfterDiscount = totalBillAfterDiscount.add(totalDiscount);
			discountDTO.setPriceAfterDiscount(totalDiscount);
			discountDTO.setAppliedDiscountInfo(category.getDiscountDescription());
			discountDTOList.add(discountDTO);
		});
		return discountDTOList;
	}
	
	

}
