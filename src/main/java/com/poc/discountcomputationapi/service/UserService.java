package com.poc.discountcomputationapi.service;

import com.poc.discountcomputationapi.model.User;

/**
*
* @author Soma
*/

public interface UserService {
	
    User getUserByName(String name);

}
