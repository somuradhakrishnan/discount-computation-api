package com.poc.discountcomputationapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.poc.discountcomputationapi.model.User;
import com.poc.discountcomputationapi.repository.UserRepository;
/**
*
* @author Soma
*/

@Service
public class UserServiceImpl implements UserService {
	
	 @Autowired
	 private UserRepository userRepository;

	@Override
	public User getUserByName(String name) {
		return userRepository.findUserByName(name);
	}
	
	

}
