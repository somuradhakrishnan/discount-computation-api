package com.poc.discountcomputationapi.validator;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.poc.discountcomputationapi.annotations.ProductConstraint;
import com.poc.discountcomputationapi.dto.BillDTO;
import com.poc.discountcomputationapi.repository.ProductRepository;

/**
 *
 * @author Soma
 */

@Component
public class ProductValidator implements ConstraintValidator<ProductConstraint, List<BillDTO>> {

	private String message;

	@Autowired
	private ProductRepository productRepository;

	@Override
	public void initialize(ProductConstraint contactNumber) {
	}

	@Override
	public boolean isValid(List<BillDTO> contactField, ConstraintValidatorContext cxt) {
		List<String> invalidProducts = new ArrayList<String>();
		cxt.disableDefaultConstraintViolation();
		contactField.stream().forEach(product -> {
			if (productRepository.findProductByName(product.getProductName().toUpperCase()) == null)
				invalidProducts.add(product.getProductName());
		});
		message = "Invalid product " + invalidProducts + ". Product can only be any one of "
				+ productRepository.findAllProductNames();
		cxt.buildConstraintViolationWithTemplate(message).addConstraintViolation();
		return invalidProducts.size() == 0;

	}

}
