package com.poc.discountcomputationapi.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.poc.discountcomputationapi.annotations.UserNameConstraint;
import com.poc.discountcomputationapi.repository.UserRepository;

public class UserNameValidator implements ConstraintValidator<UserNameConstraint, String>{
	
	private String message;
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
    public void initialize(UserNameConstraint contactNumber) {
		message = "Invalid user name. User name can only be any one of "+ userRepository.findAllUserNames();	
    }
 
    @Override
    public boolean isValid(String contactField,
      ConstraintValidatorContext cxt) {
    	cxt.disableDefaultConstraintViolation();
    	cxt.buildConstraintViolationWithTemplate(message).addConstraintViolation();

        return userRepository.findUserByName(contactField) != null;
        
    }

}
