DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS categories;
DROP TABLE IF EXISTS products;
 
CREATE TABLE categories (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  name VARCHAR(250) NOT NULL,
  priority INTEGER DEFAULT -1,
  discount_formula VARCHAR(250) NOT NULL,
  promotions  VARCHAR(250) DEFAULT NULL,
  discount_description  VARCHAR(250) DEFAULT NULL
  
);

CREATE TABLE products (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  name VARCHAR(250) NOT NULL,
  is_grocery BOOLEAN DEFAULT FALSE,
  has_discount BOOLEAN DEFAULT FALSE,
  has_promotions  BOOLEAN DEFAULT FALSE
);

CREATE TABLE users (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  first_name VARCHAR(250) NOT NULL,
  last_name VARCHAR(250) NOT NULL,
  is_employee BOOLEAN DEFAULT TRUE,
  is_affiliate BOOLEAN DEFAULT FALSE,
  customer_tenure INTEGER DEFAULT 0
);
 
INSERT INTO users (first_name, last_name,is_employee,is_affiliate,customer_tenure) VALUES
  ('Aliko', 'Dangote',TRUE,FALSE,1),
  ('Bill', 'Gates',FALSE,TRUE,1),
  ('Folrunsho', 'Alakija',FALSE,FALSE,3),
  ('Abraham', 'Lincoln',FALSE,FALSE,1);
  
INSERT INTO products (name, is_grocery) VALUES
  ('MILK', TRUE),
  ('MONITOR', FALSE);
  
INSERT INTO categories (name, discount_formula, priority, promotions, discount_description) VALUES
  ('EMPLOYEE', '((100-30)*#amount)/100.0', 1, '(#amount - (#amount%100.0))/20','Employee discount 30% + $5 for every $100' ),
  ('AFFILIATE', '((100-10)*#amount)/100.0', 2, '(#amount - (#amount%100.0))/20','Affiliate employee discount 10% + $5 for every $100'),
  ('LOYALTY_CUSTOMER', '((100-5)*#amount)/100.0', 3, '(#amount - (#amount%100.0))/20','Loyalty customer discount 5% + $5 for every $100'),
  ('FOR_ALL', '', 3, '(#amount - (#amount%100.0))/20','Discount of $5 for every $100');
  