package com.poc.discountcomputationapi;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.springframework.boot.test.context.SpringBootTest;

import com.poc.discountcomputationapi.ValidatorTest.ProductValidatorTest;
import com.poc.discountcomputationapi.controller.ControllersTest;
import com.poc.discountcomputationapi.repository.CategoryRepositoryIntegrationTest;
import com.poc.discountcomputationapi.repository.ProductRepositoryIntegrationTest;
import com.poc.discountcomputationapi.repository.UserRepositoryIntegrationTest;
import com.poc.discountcomputationapi.service.ComputationServiceImplTest;
import com.poc.discountcomputationapi.service.UserServiceImplTest;

/**
 * @author Soma
 */

@RunWith(Suite.class)
@SpringBootTest(classes = DiscountComputationApiApplication.class,
webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Suite.SuiteClasses({
	UserServiceImplTest.class,
	UserRepositoryIntegrationTest.class,
	ComputationServiceImplTest.class,
	ProductRepositoryIntegrationTest.class,
	CategoryRepositoryIntegrationTest.class,
	ProductValidatorTest.class,
	ControllersTest.class
})
public class DiscountComputationApiApplicationTests {

	@Test
	public void contextLoads() {
	}
	
	
	

}
