package com.poc.discountcomputationapi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import com.poc.discountcomputationapi.service.UserService;
import com.poc.discountcomputationapi.service.UserServiceImpl;

/**
*
* @author Soma
*/

@Profile("test")
@Configuration
public class ServiceTestConfiguration {

	@Bean
	@Primary
	public UserService userService() {
		return new UserServiceImpl();
	}
}
