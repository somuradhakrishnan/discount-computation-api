package com.poc.discountcomputationapi.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.poc.discountcomputationapi.DiscountComputationApiApplication;
import com.poc.discountcomputationapi.ValidatorTest.JsonUtil;
import com.poc.discountcomputationapi.dto.BillDTO;
import com.poc.discountcomputationapi.dto.PostDTO;
import com.poc.discountcomputationapi.service.ComputationService;

/**
*
* @author Soma
*/

@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class ControllersTest {
	
	@Autowired
    private MockMvc mvc;
 
    @MockBean
    private ComputationService computationService;
    
    PostDTO postDTO = new PostDTO();
    
    @Before
    public void setUp() {
    	postDTO.setUser("Aliko");
        postDTO.setBillNo("A123");
        postDTO.setTotal(new BigDecimal(990));
        postDTO.setBillDto(new ArrayList<BillDTO>());
        BillDTO billDTO = new BillDTO();
        billDTO.setId(1);
        billDTO.setPriceOfEach(new BigDecimal(99));
        billDTO.setProductName("MILK");
        billDTO.setQuantity(10);
        billDTO.setTotalPrice(new BigDecimal(990));
        postDTO.getBillDto().add(billDTO);
    }
    
    @Test
    public void givenDiscountdetailsRequest_whenValidDetails_thenReturnOKStatus()
      throws Exception {
        mvc.perform(post("/api/v1/getDiscountDetails").contextPath("/api/v1").contentType(MediaType.APPLICATION_JSON).content(JsonUtil.toJson(postDTO))).andExpect(status().is(200));
        
    }
    
    
    

}
