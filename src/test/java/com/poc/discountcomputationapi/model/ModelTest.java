package com.poc.discountcomputationapi.model;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

/**
*
* @author Soma
*/

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class ModelTest {

	User user = new User();
	Product product = new Product();
	Category category = new Category();
	
	@Test
	public void userModelTest() throws IOException {
		user.setId(1);
		user.setCustomerTenure(2);
		user.setFirstName("Test1");
		user.setLastName("Test2");
		user.setIsEmployee(true);
		user.setIsAffiliate(false);
		Assert.assertEquals(user.getFirstName(), "Test1");
		
	}
	
	
	@Test
	public void productModelTest() throws IOException {
		product.setId(1);
		product.setHasDiscount(true);
		product.setId(1);
		product.setIsGrocery(true);
		product.setName("Test");
		product.setHasPromotions(true);
		Assert.assertEquals(product.getId(), 1);
		Assert.assertEquals(product.getHasPromotions(), true);
		Assert.assertEquals(product.getHasDiscount(), true);
		Assert.assertEquals(product.getName(), "Test");
		
	}
	
	
	@Test
	public void categoryModelTest() throws IOException {
		category.setId(1);
		category.setDiscountDescription("Test");
		category.setName("Test");
		category.setPriority(5);
		category.setPromotions("Test");
		category.setDiscountFormula("Test");
		Assert.assertEquals(category.getDiscountFormula(), "Test");
		
	}

}
