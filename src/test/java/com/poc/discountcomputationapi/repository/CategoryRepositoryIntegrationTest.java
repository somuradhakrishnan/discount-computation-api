package com.poc.discountcomputationapi.repository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.poc.discountcomputationapi.model.Category;

/**
*
* @author Soma
*/

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class CategoryRepositoryIntegrationTest {
	
	@Autowired
	CategoryRepository categoryRepository;
	
	@Test
	 public void categoryRepositoryTestForCategoryByName() throws Exception {
		Category category = categoryRepository.findCategoryByName("EMPLOYEE");
	   Assert.assertNotNull(category);
	 }

}
