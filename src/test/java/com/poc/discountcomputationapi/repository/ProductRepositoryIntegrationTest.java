package com.poc.discountcomputationapi.repository;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.poc.discountcomputationapi.model.Product;
import com.poc.discountcomputationapi.model.User;

/**
*
* @author Soma
*/

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class ProductRepositoryIntegrationTest {

	@Autowired
	ProductRepository productRepository;
	
	@Test
	 public void productRepositoryTestForFindAllProducts() throws Exception {
	    List<String> products = productRepository.findAllProductNames();
	    Assert.assertNotNull(products);
	 }
	
	@Test
	 public void productRepositoryTestForProductByName() throws Exception {
	   Product product = productRepository.findProductByName("MILK");
	   Assert.assertNotNull(product);
	 }
	
}
