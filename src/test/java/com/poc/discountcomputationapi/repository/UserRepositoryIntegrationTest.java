package com.poc.discountcomputationapi.repository;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.poc.discountcomputationapi.model.User;

/**
*
* @author Soma
*/

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class UserRepositoryIntegrationTest {
	 @Autowired
	 UserRepository userRepository;

	 @Test
	 public void userRepositoryTestForUserByName() throws Exception {
	   User user = userRepository.findUserByName("Aliko");
	   Assert.assertNotNull(user);
	 }
	 
	 @Test
	 public void userRepositoryTestForFindAllUsers() throws Exception {
	    List<String> users = userRepository.findAllUserNames();
	    Assert.assertNotNull(users);
	 }
	 

}
