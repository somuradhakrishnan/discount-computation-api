package com.poc.discountcomputationapi.service;

import java.math.BigDecimal;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.poc.discountcomputationapi.dto.BillDTO;
import com.poc.discountcomputationapi.dto.PostDTO;

/*
* @author Soma
*/

@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class ComputationServiceImplTest {
	
	@Autowired
	ComputationService computationService;
	
	PostDTO postDTO = new PostDTO();
	
	ObjectMapper mapper = new ObjectMapper();
	
	@Test
	public void whenPostDTOProvided_thenCalculateDiscountForEmployee() throws JsonProcessingException{
		PostDTO postDTO = new PostDTO();
		BillDTO billDTO = new BillDTO();
		postDTO.setBillNo("A123");
		postDTO.setTotal(new BigDecimal(993));
		postDTO.setUser("Aliko");
		billDTO.setId(1);
		billDTO.setPriceOfEach(new BigDecimal(99.3));
		billDTO.setProductName("MONITOR");
		billDTO.setQuantity(10);
		billDTO.setTotalPrice(new BigDecimal(993));
		postDTO.setBillDto(Arrays.asList(billDTO));
		String result = "{\"user\":\"Aliko\",\"billNo\":\"A123\",\"discountDTO\":[{\"id\":1,\"priceAfterDiscount\":650.1,\"appliedDiscountInfo\":\"Employee discount 30% + $5 for every $100\"}],\"total\":650.1}";
		Assert.assertEquals(mapper.writeValueAsString(computationService.getDiscountDetails(postDTO)),result);
	}
	
	@Test
	public void whenPostDTOProvided_thenGetCalculateDiscountForAffiliate() throws JsonProcessingException{
		PostDTO postDTO = new PostDTO();
		BillDTO billDTO = new BillDTO();
		postDTO.setBillNo("A123");
		postDTO.setTotal(new BigDecimal(993));
		postDTO.setUser("Bill");
		billDTO.setId(1);
		billDTO.setPriceOfEach(new BigDecimal(99.3));
		billDTO.setProductName("MONITOR");
		billDTO.setQuantity(10);
		billDTO.setTotalPrice(new BigDecimal(993));
		postDTO.setBillDto(Arrays.asList(billDTO));
		String result = "{\"user\":\"Bill\",\"billNo\":\"A123\",\"discountDTO\":[{\"id\":1,\"priceAfterDiscount\":848.7,\"appliedDiscountInfo\":\"Affiliate employee discount 10% + $5 for every $100\"}],\"total\":848.7}";
		Assert.assertEquals(mapper.writeValueAsString(computationService.getDiscountDetails(postDTO)),result);
	}
	
	@Test
	public void whenPostDTOProvided_thenGetCalculateDiscountForLC() throws JsonProcessingException{
		PostDTO postDTO = new PostDTO();
		BillDTO billDTO = new BillDTO();
		postDTO.setBillNo("A123");
		postDTO.setTotal(new BigDecimal(993));
		postDTO.setUser("Folrunsho");
		billDTO.setId(1);
		billDTO.setPriceOfEach(new BigDecimal(99.3));
		billDTO.setProductName("MONITOR");
		billDTO.setQuantity(10);
		billDTO.setTotalPrice(new BigDecimal(993));
		postDTO.setBillDto(Arrays.asList(billDTO));
		String result = "{\"user\":\"Folrunsho\",\"billNo\":\"A123\",\"discountDTO\":[{\"id\":1,\"priceAfterDiscount\":898.4,\"appliedDiscountInfo\":\"Loyalty customer discount 5% + $5 for every $100\"}],\"total\":898.4}";
		Assert.assertEquals(mapper.writeValueAsString(computationService.getDiscountDetails(postDTO)),result);
	}
	
	@Test
	public void whenPostDTOProvided_thenGetCalculateDiscountForTenureLessthen2() throws JsonProcessingException{
		PostDTO postDTO = new PostDTO();
		BillDTO billDTO = new BillDTO();
		postDTO.setBillNo("A123");
		postDTO.setTotal(new BigDecimal(993));
		postDTO.setUser("Abraham");
		billDTO.setId(1);
		billDTO.setPriceOfEach(new BigDecimal(99.3));
		billDTO.setProductName("MONITOR");
		billDTO.setQuantity(10);
		billDTO.setTotalPrice(new BigDecimal(993));
		postDTO.setBillDto(Arrays.asList(billDTO));
		String result = "{\"user\":\"Abraham\",\"billNo\":\"A123\",\"discountDTO\":[{\"id\":1,\"priceAfterDiscount\":948.0,\"appliedDiscountInfo\":\"Discount of $5 for every $100\"}],\"total\":948.0}";
		Assert.assertEquals(mapper.writeValueAsString(computationService.getDiscountDetails(postDTO)),result);
	}
	
	@Test
	public void whenPostDTOProvided_thenGetCalculateDiscountForGroceryProducts() throws JsonProcessingException{
		PostDTO postDTO = new PostDTO();
		BillDTO billDTO = new BillDTO();
		postDTO.setBillNo("A123");
		postDTO.setTotal(new BigDecimal(993));
		postDTO.setUser("Folrunsho");
		billDTO.setId(1);
		billDTO.setPriceOfEach(new BigDecimal(99.3));
		billDTO.setProductName("MILK");
		billDTO.setQuantity(10);
		billDTO.setTotalPrice(new BigDecimal(993));
		postDTO.setBillDto(Arrays.asList(billDTO));
		String result = "{\"user\":\"Folrunsho\",\"billNo\":\"A123\",\"discountDTO\":[{\"id\":1,\"priceAfterDiscount\":948.0,\"appliedDiscountInfo\":\"Loyalty customer discount 5% + $5 for every $100\"}],\"total\":948.0}";
		Assert.assertEquals(mapper.writeValueAsString(computationService.getDiscountDetails(postDTO)),result);
	}
	
	@Test
	public void getCategoryDetailsTest() throws JsonProcessingException{
		String result = "{\"id\":3,\"name\":\"LOYALTY_CUSTOMER\",\"priority\":3,\"discountFormula\":\"((100-5)*#amount)/100.0\",\"promotions\":\"(#amount - (#amount%100.0))/20\",\"discountDescription\":\"Loyalty customer discount 5% + $5 for every $100\"}";
		Assert.assertEquals(mapper.writeValueAsString(computationService.getDiscountFormulas("LOYALTY_CUSTOMER")),result);
		
	}

}
