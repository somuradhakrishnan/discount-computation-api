package com.poc.discountcomputationapi.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.poc.discountcomputationapi.model.User;
import com.poc.discountcomputationapi.repository.UserRepository;

/**
*
* @author Soma
*/

@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class UserServiceImplTest {

	@Autowired
	private UserService userService;

	@MockBean
	private UserRepository userRepository;

	ObjectMapper mapper = new ObjectMapper();

	@Before
	public void setUp() {
		User test = new User("FirstTest", "LastTest", false, false, 0);
		Mockito.when(userRepository.findUserByName(test.getFirstName())).thenReturn(test);
	}

	@Test
	public void whenUserNameIsProvided_thenRetrievedDataIsCorrect() throws JsonProcessingException{
		String name = "FirstTest";
		String result = "{\"id\":0,\"firstName\":\"FirstTest\",\"lastName\":\"LastTest\",\"isEmployee\":false,\"isAffiliate\":false,\"customerTenure\":0}";
		User user = userService.getUserByName(name);
		Assert.assertEquals(mapper.writeValueAsString(user), result);
	}

}
